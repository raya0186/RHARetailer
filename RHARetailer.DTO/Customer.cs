﻿using System;

namespace RHARetailer.DTO
{
    public class Customer
    {
        public int CustomerNo { get; set; }
        public string email { get; set; }
        public string Password { get; set; }
        public int PhoneNumber { get; set; }
        public string Address { get; set; }

    }
}
