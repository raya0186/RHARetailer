﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RHARetailer.DTO
{
    class Product
    {
        public int ProductNo { get; set; }
        public string ProductName { get; set; }
        public int stock { get; set; }

    }
}
