﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RHARetailer.DTO
{
    class Order
    {
        public int CustomerNo { get; set; }
        public int OrderNo { get; set; }
        public string Product { get; set; }

    }
}
